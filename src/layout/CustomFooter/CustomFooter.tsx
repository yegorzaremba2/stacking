import { FC } from "react";

import { Layout, Typography } from "antd";

import discord_Logo from "assets/svg/social/discord.svg";
import telegram_Logo from "assets/svg/social/telegram.svg";
import twitter_Logo from "assets/svg/social/twitter.svg";
import { useWindowSize } from "hooks";
import { color, fontFamily } from "layout/constants";
const { Text } = Typography;
const { Footer } = Layout;

const CustomFooter: FC = () => {
  const { isTablet } = useWindowSize();

  return (
    <Footer style={{
        textAlign: "center",
        width: "100%",
        backgroundColor: "transparent",
        display: 'flex',
        flexDirection: isTablet ? 'column' : 'row',
        justifyContent: isTablet ? 'center' : 'space-between',
    }}>
      <div>
        <Typography>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.875em', fontWeight: 700 }}>
            Grimace&nbsp;
          </Text>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.875em', fontWeight: 300 }}>
            STAKING&nbsp;
          </Text>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.875em', fontWeight: 300 }}>
            © Grimace Coin 2024
          </Text>
        </Typography>
      </div>

      <div style={{ display: 'flex', paddingTop: isTablet ? '0.5em' : 0, flexDirection: isTablet ? 'column' : 'row', justifyContent: 'center', alignContent: 'center' }}>
        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300, paddingRight: '0.4em' }}>SWAP |&nbsp;</Text>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300, paddingRight: '0.4em' }}>NFT |&nbsp;</Text>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300, paddingRight: '0.4em' }}>WHITEPAPER |&nbsp;</Text>
          <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300, paddingRight: '1em' }}>GRIMACE</Text>
        </div>

        <div style={{ display: 'flex', paddingTop: isTablet ? '1em' : 0, flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
          <div style={{ paddingRight: '1em' }}>
            <img src={discord_Logo} style={{ width: '2.25em', height: '2.25em' }} alt="web3-wallet" />
          </div>
          <div style={{ paddingRight: '1em' }}>
            <img src={telegram_Logo} style={{ width: '2.25em', height: '2.25em' }} alt="web3-wallet" />
          </div>
          <div>
            <img src={twitter_Logo} style={{ width: '2.25em', height: '2.25em' }} alt="web3-wallet" />
          </div>
        </div>
      </div>
    </Footer>
  );
};

export default CustomFooter;
