import { FC } from "react";

import { useWindowSize } from "hooks";

type MainContentProps = {
  children?: React.ReactNode;
};

const styles = {
  content: {
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    marginTop: "5em",
    padding: "1em 4em 2em 4em",
    overflow: "auto",
    flex: 1,
  },
  contentMobile: {
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    marginTop: "5em",
    padding: "0.5em 0 0.5em",
    overflow: "hidden",
    flex: 1,
  }
} as const;

const MainContent: FC<MainContentProps> = ({ children }) => {
  const { isTablet } = useWindowSize();

  return <div style={isTablet ? styles.contentMobile : styles.content}>{children}</div>;
};

export default MainContent;
