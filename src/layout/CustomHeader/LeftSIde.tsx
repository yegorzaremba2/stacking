import { FC } from "react";

import { Typography } from "antd";
import { Link } from "react-router-dom";

import { useWindowSize } from "hooks";
import { color, fontFamily } from "layout/constants";
const { Text } = Typography;

export const LeftSide: FC = () => {
    const { isTablet } = useWindowSize();

    return (
      <div style={{ padding: isTablet ? '1em 0 0 1em' : '2em 0 0 2em' }}>
          <Link to="/">
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <Typography>
                <Text style={{ color, fontFamily, fontSize: isTablet ? '1.2em' : '2.75em', fontWeight: 700 }}>
                  GRIMACE&nbsp;
                </Text>
                <Text style={{ color, fontFamily, fontSize: isTablet ? '1.2em' : '2.75em', fontWeight: 300 }}>
                  STACKING
                </Text>
              </Typography>
            </div>
          </Link>


          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <Typography>

              <Link to="/dashboard">
                <Text style={{ color, fontFamily, fontSize: isTablet ? '0.8em' : '1.8em', fontWeight: 300, paddingRight: '1em' }}>
                  DASHBOARD
                </Text>
              </Link>
              <Text style={{ color, fontFamily, fontSize: isTablet ? '0.8em' : '1.8em', fontWeight: 300, paddingRight: '1em' }}>
                RANK
              </Text>
              <Text style={{ color, fontFamily, fontSize: isTablet ? '0.8em' : '1.8em', fontWeight: 300, paddingRight: '1em' }}>
                FAQ
              </Text>
            </Typography>
          </div>
      </div>
    );
  };
  