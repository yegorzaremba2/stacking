import { FC } from "react";

import { Layout } from "antd";

import { SeedAccount } from "components/Account/SeedAccount";
import { useWindowSize } from "hooks";

import { LeftSide } from "./LeftSIde";

const { Header } = Layout;

const styles = {
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    backgroundColor: "#1A1A1A",
    paddingTop: "1em",
    zIndex: 1
  },
  headerRight: {
    display: "flex",
    gap: "10px",
    alignItems: "center",
    paddingRight: "10px",
    fontSize: "15px",
    fontWeight: "600"
  }
} as const;

const CustomHeader: FC = () => {
  const { isTablet } = useWindowSize();

  return (
    <Header style={{ ...styles.header, padding: isTablet ? "15px 5px 0 5px" : "15px 20px 0 20px" }}>
      <LeftSide />

      <div style={styles.headerRight}>
        <SeedAccount />
      </div>
    </Header>
  );
};

export default CustomHeader;
