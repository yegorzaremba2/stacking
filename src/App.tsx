import { Buffer } from "buffer";

import { useState } from "react";

import { Layout, ConfigProvider, theme } from "antd";
import { Route, Routes } from "react-router-dom";

import { CustomHeader, MainContent, CustomFooter } from "layout";
import "styles/App.css";
import { Dashboard } from "components/Dashboard/Dashboard";
import { StackingFlow } from "components/StackingFlow/StackingFlow";

const styles = {
  layout: {
    width: "100%",
    height: "100%",
    minHeight: '100vh',
    overflow: "auto",
    fontFamily: "Sora, sans-serif",
    backgroundColor: '#1A1A1A',
    display: 'flex',
    flexDirection: 'column',
  }
} as const;

function App() {
  const { defaultAlgorithm, darkAlgorithm } = theme;
  const [isDarkMode] = useState(false);
  if (!window.Buffer) window.Buffer = Buffer;

  return (
    <ConfigProvider
      theme={{
        algorithm: isDarkMode ? darkAlgorithm : defaultAlgorithm
      }}
    >
      <Layout style={styles.layout}>
        <CustomHeader />
        <MainContent>
          <Routes>
            <Route path="/" >
              <Route index element={<StackingFlow />} />
              <Route path="dashboard" element={<Dashboard />} />
            </Route>
          </Routes>
        </MainContent>
        <CustomFooter />
      </Layout>
    </ConfigProvider>
  );
}

export default App;
