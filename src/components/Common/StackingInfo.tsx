import React from "react";

import { Typography } from "antd";

const { Text } = Typography;
import { color, fontFamily } from "layout/constants";

import { formatDate } from "./utils";

const grey = '#b0aeae'

export type IStackingInfo = {
    finishDate: Date;
    deposit: number
    withdrawal: number;
    period: string;
}

export const StackingInfo: React.FC<{data: IStackingInfo}> = ({ data }) => {
  return (
    <div style={{ color: grey, fontWeight: 300, fontFamily, display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'left', justifyContent: 'space-between' }}>
            <div style={{ width: '100%' }}>
                <div style={{ fontSize: '1.4em' }}>Deposit amount</div>
                <div style={{ display: 'flex', flexDirection: 'row', }}>
                    <Text style={{ color, fontFamily, fontSize: '2em', fontWeight: 400 }}>{data.deposit}&nbsp;</Text>
                    <Text style={{paddingBottom: '0.5em', alignSelf: 'flex-end', color, fontFamily, fontSize: '1em', }}>GRIMACE</Text>
                </div>
            </div>

            <div style={{ width: '100%' }}>
                <div style={{ fontSize: '1.4em' }}>Withdrawal amount</div>
                <div style={{ display: 'flex', flexDirection: 'row', }}>
                    <Text style={{ color, fontFamily, fontSize: '2em', fontWeight: 400 }}>{data.withdrawal}&nbsp;</Text>
                    <Text style={{ paddingBottom: '0.5em', alignSelf: 'flex-end', color, fontFamily, fontSize: '1em', }}>GRIMACE</Text>
                </div>
            </div>
        </div>
        
        <div style={{ height: '1em' }} />
        
        <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'left', justifyContent: 'space-between' }}>
            <div style={{ width: '100%' }}>
                <div style={{ fontSize: '1.4em' }}>Period</div>
                <div style={{ display: 'flex', flexDirection: 'row', }}>
                    <Text style={{ color, fontFamily, fontSize: '2em', fontWeight: 400 }}>{data.period}</Text>
                </div>
            </div>
            <div style={{ width: '100%' }}>
                <div style={{ fontSize: '1.4em' }}>Unstake date:</div>
                <div style={{ display: 'flex', flexDirection: 'row', }}>
                    <Text style={{ color, fontFamily, fontSize: '2em', fontWeight: 400 }}>{formatDate(data.finishDate)}</Text>
                </div>
            </div>
        </div>
    </div>
  )
};
