/* eslint-disable @typescript-eslint/no-var-requires */
export const tokenABI = require('./contracts/test/Token.sol/TestToken.json')
export const stackingABI = require('./contracts/Stacking.sol/Stacking.json')

export const formatDate = (_data: Date) => {
    const date = _data.toISOString()
    const year = date.slice(0,4)
    const month = date.slice(5,7)
    const day = date.slice(8,10)
    const hours = date.slice(11,13)
    const minutes = date.slice(14,16)
    return `${day}/${month}/${year} ${hours}:${minutes}`
}

export const tokenAddress = '0x51726d42b926cb7FfF9b77E62Ca2d7E54C922a17'
export const stackingAddress = '0x740d569B617d80938866f97Dd17C09823Ff26840'
