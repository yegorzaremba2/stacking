/* eslint-disable prefer-const */
/* eslint-disable no-empty */
export const waitConfirmation = (provider: any, txHash: string) => {
    return new Promise((resolve, reject) => {
      let timeout: string | number | NodeJS.Timeout | undefined;
      const interval = setInterval(async () => {
        try {
          const receipt = await provider.getTransactionReceipt(txHash);

          if (receipt) {
            const { blockNumber, status } = receipt;
            const currentBlock = await provider.getBlockNumber();

            if (currentBlock >= Number(blockNumber) + 1) {
              clearInterval(interval);
              clearTimeout(timeout);

              if (!status) reject(new Error('tx was failed'));

              resolve(receipt);
            }
          }
        } catch (error) {}
      }, 3000);

      timeout = setTimeout(() => {
        clearInterval(interval);
        clearTimeout(timeout);
        reject(new Error('Timeout'));
      }, 5 * 60 * 1000);
    });
}