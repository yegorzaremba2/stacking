/* eslint-disable import/order */
import React from "react";

import { Typography } from "antd";
const { Text } = Typography;

import { ethers } from "ethers";

import { StackingInfo } from "components/Common/StackingInfo";
import { stackingABI, stackingAddress } from "components/Common/utils";
import { waitConfirmation } from "components/Common/waitingConfirmation";
import { WithdrawButton, WithdrawEarlyButton } from "components/StackingFlow/steps/StepButton";
import { useSignerOrProvider, useWindowSize } from "hooks";
import { color, fontFamily } from "layout/constants";

export const DepositEarly: React.FC<any> = ({ deposit, setRefresh }: any) => {
  const { isTablet } = useWindowSize();
  const { provider, signer } = useSignerOrProvider();

  const withdraw = async (deposit: any) => {
    const staclingContract = new ethers.Contract(stackingAddress, stackingABI.abi, signer)
    const tx = await staclingContract.withdraw(deposit.index)
    await waitConfirmation(provider, tx.hash)
    setRefresh(true)
  }

  return (
            <div style={{
                minWidth: "560px",
                textAlign: "center",
                borderRadius: "10px",
                display: 'flex',
                flexDirection: 'column',
                border: '0.25em solid #542297',
                padding: '0.875em 2.625em 3.125em 2.625em',
                color,
                background: 'linear-gradient(0deg, #7A30DC, #7A30DC)'
            }}>
                <div style={{ 
                    textAlign: 'left', 
                    paddingTop: '0.625em', 
                    display: 'flex', 
                    flexDirection: 'column', 
                    justifyContent: 'left', 
                    width: '100%' 
                }}>
                    <Text style={{ opacity: 0.9, color, fontFamily, fontSize: isTablet ? '1.4em' : '2.3875em', fontWeight: 300 }}>
                        DEPOSIT #{deposit.index + 1}
                    </Text>
                    <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '2em', fontWeight: 700 }}>
                        {deposit.isTaken ? "ALREADY TAKEN" : "NOT READY TO WITHDRAWALS"}
                    </Text>
                </div>
                <div style={{ height: '0.5em' }} />
                {Object.keys(deposit).length > 1 && <StackingInfo data={deposit} />}
                <div style={{ height: '2em' }} />

                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                    {deposit.isTaken
                        ? <WithdrawButton title={'STAKE AGAIN'} withdraw={() => {}} background={'linear-gradient(270deg, #DC8D30 4.47%, #DC6E30 100%)'} /> 
                        : <WithdrawEarlyButton title={'EARLY UNSTAKE'} withdraw={() => withdraw(deposit)} background={'linear-gradient(270deg, #DC3030 4.47%, #5B151D 100%)'} />}
                </div>
        </div>
  )
};


