import React, { useEffect, useState } from "react";

import { useWeb3React } from "@web3-react/core";
import { Typography } from "antd";
const { Text } = Typography;
import { ethers } from "ethers";

import { IStackingInfo } from "components/Common/StackingInfo";
import { stackingABI, stackingAddress } from "components/Common/utils";
import { useWindowSize } from "hooks";
import { color, fontFamily } from "layout/constants";

import { DepositEarly } from "./DepositEarly";
import { DepositReady } from "./DepositReady";

export const Dashboard: React.FC = () => {
  const { provider, account } = useWeb3React();
  const { isMobile } = useWindowSize();
  const [depositArr, setDepositArr] = useState([] as IStackingInfo[]);
  const [isRefresh, setRefresh] = useState(true);

  const plans = {
    1: '3 month',
    2: '6 months',
    3: '1 years',
    4: '5 years',
  }

  const plansAPR = {
    1: { year: 0.25, percent: 0.11 },
    2: { year: 0.5, percent: 0.22 },
    3: { year: 1, percent: 0.66 },
    4: { year: 5, percent: 1.2 },
}

  useEffect(() => {
    const getInfo = async () => {
        const stackingContract = new ethers.Contract(stackingAddress, stackingABI.abi, provider)
        const rawDepositAmount = await stackingContract.callStatic.getUserAmountOfDeposits(account)
        const depositAmount = Number(rawDepositAmount.toString())

        const depositArray = await Promise.all(new Array(depositAmount).fill("*").map(async (_, index) => {
            const rawStacking = await stackingContract.callStatic.getUserDepositInfo(account, index)

            const stacking = {
                depositId: index + 1,
                isTaken: rawStacking.isTaken,
                start: rawStacking.start.toString(),
                amount: rawStacking.amount,
                finish: rawStacking.finish.toString(),
                percent: rawStacking.percent.toString(),
                plan: Number(rawStacking.plan)
            }

            // @ts-expect-error skip
            const { year, percent } = plansAPR[stacking.plan + 1]
            const amount = Number(Number(ethers.utils.formatEther(stacking.amount).toString()).toFixed(4))


            return {
                deposit: Number(ethers.utils.formatEther(stacking.amount).toString()),
                withdrawal: Number((amount - (amount * percent / 100 * year)).toFixed(4)),
                finishDate: new Date(1000 * Number(stacking.finish)),
                isTaken: stacking.isTaken,
                // @ts-expect-error skip
                period: plans[stacking.plan + 1],
            }
        }))

        setDepositArr(depositArray)
    }   
    if (account) {
        if (isRefresh) {
            setRefresh(false)
            getInfo()
        }
    }
}, [account, isRefresh])

  return (
        <div style={{ 
            textAlign: "center",
            margin: "auto",
            padding: "0 0 1em 0",
            borderRadius: "10px", 
            width: isMobile ? "80%" : "100%" 
        }}>
            <div style={{ textAlign: 'right', display: 'flex', paddingBottom: '1em', flexDirection: 'column', justifyContent: 'left', width: '100%' }}>
                <Text style={{ color, fontFamily, fontSize: isMobile ? '1em' : '2em', fontWeight: 700 }}>
                    MY DASHBOARD
                </Text>
                <Text style={{ opacity: 0.9, color, fontFamily, fontSize: isMobile ? '0.6em' : '1.2em', fontWeight: 300 }}>
                    {account}
                </Text>
            </div>

            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {[...depositArr.map((el, index) => {
                    return el.finishDate > new Date() ? 
                        <DepositEarly key={index} setRefresh={setRefresh} deposit={{...el, index}} /> : 
                        <DepositReady key={index} setRefresh={setRefresh} deposit={{...el, index}} />
                })]}
            </div>
        </div>
  )
};


