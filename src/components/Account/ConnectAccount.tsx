import React, { useState } from "react";

import { useWeb3React } from "@web3-react/core";
import { Button, Typography } from "antd";

import { color, fontFamily } from "layout/constants";

import ConnectModal from "./ConnectModal";
const { Text } = Typography;

interface WantedChain {
  chain?: number;
}

export const ConnectAccount: React.FC<WantedChain> = () => {
  const { account } = useWeb3React();
  const [isAuthModalOpen, setIsAuthModalOpen] = useState<boolean>(false);

  return (
    <>
      {account === undefined ? (
        <div>
          <Button 
            style={{ 
              color, 
              fontFamily, 
              background: 'linear-gradient(270deg, #FFAA47 4.47%, #926015 100%)',
              height: '3.125em',
              width: '18.75em',
            }} 
            onClick={() => setIsAuthModalOpen(true)}
          >
            <Text style={{ color, fontFamily, fontSize: '1.5em', fontWeight: '300' }}>
              CONNECT
            </Text>
          </Button>
          <ConnectModal isModalOpen={isAuthModalOpen} setIsModalOpen={setIsAuthModalOpen} />
          <br />
        </div>
      ) : null}
    </>
  );
};
