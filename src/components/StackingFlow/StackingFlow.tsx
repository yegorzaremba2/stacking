import { useEffect, useState } from "react";

import { useWeb3React } from "@web3-react/core";

import { FirstStep } from "./steps/First";
import { IntroStep } from "./steps/Intro";
import { SecondStep } from "./steps/Second";
import { ThirdStep } from "./steps/Third";

export const StackingFlow: React.FC = () => {
  const { account } = useWeb3React();

  const [step, setStep] = useState(0)
  // const [step] = useState(3)
  const [amount, setAmount] = useState(0)
  const [plan, setPlan] = useState(0)

  useEffect(() => {
    if (account) {
      if (amount > 0) {
        if (plan > 0) {
          setStep(3)
        } else {
          setStep(2)
        }
      } else {
        setStep(1)
      }
    } else {
      setStep(0)
    }
  }, [account, amount, plan])

  if (step === 0) return <IntroStep />
  if (step === 1) return <FirstStep setAmount={setAmount} />
  if (step === 2) return <SecondStep setPlan={setPlan} />
  if (step === 3) return <ThirdStep amount={amount} plan={plan} />

  return null
};
