import { useEffect, useState } from "react";

import { useWeb3React } from "@web3-react/core";
import { Input, Typography } from "antd";
import { ethers } from "ethers";

import bitget_Logo from "assets/svg/grimace/bitget.svg";
import swap_Logo from "assets/svg/grimace/swap.svg";
import { tokenABI, tokenAddress } from "components/Common/utils";
import { useWindowSize } from "hooks";
import { black, color, fontFamily } from "layout/constants";

import { StepButton } from "./StepButton";

const { Text } = Typography;

const styles = {
  container: {
    width: "80%",
    minWidth: "330px",
    maxWidth: "588px",
    textAlign: "center",
    margin: "auto",
    padding: "20px 0",
    borderRadius: "10px"
  },
  input: {
    color: black
  }
} as const;

export const FirstStep = ({ setAmount }: any) => {

    const { account, provider } = useWeb3React();
    const { isTablet } = useWindowSize();
    const [balance, setBalance] = useState(0)
    const [grimace, setGrimace] = useState(0)

    useEffect(() => {
        const getBalance = async () => {
            const tokenContract = new ethers.Contract(tokenAddress, tokenABI.abi, provider)
            const balance = await tokenContract.callStatic.balanceOf(account)
            const amount = ethers.utils.formatEther(balance)
            setBalance(Number(Number(amount).toFixed(4)))
        }
        if (account) {
            getBalance()
        }
    }, [account])

    return (
        <div style={{ ...styles.container, width: isTablet ? "90%" : "80%" }}>
            <div style={{ 
                display: 'flex',
                flexDirection: 'column',
                border: '0.25em solid #542297',
                padding: '0 2.625em 3em 2.625em',
                color
            }}>
                <div style={{ paddingTop: '0.625em', display: 'flex', flexDirection: 'row', justifyContent: 'left', width: '100%', }}>
                    <Text style={{ color, fontFamily, fontSize: isTablet ? '2em' : '3em', fontWeight: 400 }}>STEP 1:&nbsp;</Text>
                </div>
                <Text style={{ textAlign: 'left', opacity: 0.5, color, fontFamily, fontSize: isTablet ? '1.2em' : '2em', fontWeight: 300 }}>ENTER AMOUNT</Text>

                <div style={{ height: '1em' }} />

                <Input onChange={(e) => setGrimace(Number(e.target.value))} style={{ fontFamily, fontSize: '2em', borderRadius: '1.5em', height: '2em', backgroundColor: '#262626', color }} placeholder="Enter amount" />
                
                <div style={{ height: '1em' }} />

                <div style={{ fontFamily, fontSize: '1.2em', textAlign: 'right' }}>
                    CURRENCT BALANCE: {balance} GRIMACE
                </div>

                <div style={{ height: '2em' }} />

                <div>
                    <StepButton title={'STEP 2'} setAmount={setAmount} grimace={grimace} />
                </div>
            </div>

            <div style={{ height: '10px' }} />

            <div style={{ textAlign: 'left' }}>
                <Text style={{ opacity: 0.5, color, fontFamily, fontSize: isTablet ? '1.2em' : '2em', fontWeight: 300 }}>Still no GRIMACE?</Text>
            </div>

            <div style={{ height: '10px' }} />

            <div style={{
                backgroundColor: '#ffffff',
                padding: '0 0.7em 0 0.15em',
                border: '0.25em solid #055257',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                height: '6em'
            }}>
                <img src={bitget_Logo} style={{ width: '5.625em', height: '5.625em' }} alt="web3-wallet" />
                <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'right' }}>
                <div style={{ height: '3em', display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', color: black, fontFamily }}>BitGet&nbsp;</Text>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', fontWeight: 300, color: black, fontFamily }}>EXCHANGE</Text>
                </div>
                <Text style={{ opacity: .7, fontSize: isTablet ? '1em' : '1.25em', fontWeight: 300, color: black, fontFamily }}>BUY GRIMACE TOKEN ON BITGET CEX</Text>
                </div>
            </div>

            <div style={{ height: '10px' }} />

            <div style={{
                background: 'linear-gradient(90deg, #7A30DC -3.06%, #271145 100%)',
                padding: '0 0.7em 0 0.15em',
                border: '0.25em solid #32155B',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                height: '6em'
            }}>
                <img src={swap_Logo} style={{ width: '5.625em', height: '5.625em' }} alt="web3-wallet" />

                <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'right' }}>
                <div style={{ height: '3em', display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', color, fontFamily }}>GRIMACE&nbsp;</Text>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', fontWeight: 300, color, fontFamily }}>SWAP</Text>
                </div>
                    <Text style={{ opacity: .7, fontSize: isTablet ? '1em' : '1.25em', fontWeight: 300, color, fontFamily }}>SWAP GRIMACE ON ARBITRUM</Text>
                </div>
            </div>
        </div>
    );
}
