import { Button } from "antd"

import { fontFamily } from "layout/constants"

export const StepButton = ({ title, setAmount, grimace }: any) => {
    return (
        <Button onClick={() => setAmount(grimace)} style={{
            fontFamily,
            height: '2em',
            fontSize: '2em',
            width: '10em',
            borderColor: '#32155B',
            borderRadius: '1em',
            background: 'linear-gradient(270deg, #7A30DC 4.47%, #32155B 100%)',
            color: '#ffffff' 
        }}>{title}</Button>
    )
}

export const StepButton2 = ({ title, setPlan, plan }: any) => {
    return (
        <Button onClick={() => setPlan(plan)} style={{
            fontFamily,
            height: '2em',
            fontSize: '2em',
            width: '10em',
            borderColor: '#32155B',
            borderRadius: '1em',
            background: 'linear-gradient(270deg, #7A30DC 4.47%, #32155B 100%)',
            color: '#ffffff' 
        }}>{title}</Button>
    )
}

export const StakeButton = ({ title, stake }: any) => {
    return (
        <Button onClick={() => stake()} style={{
            fontFamily,
            height: '2em',
            fontSize: '2em',
            width: '10em',
            borderColor: '#32155B',
            borderRadius: '1em',
            background: 'white',
            color: '#7A30DC' 
        }}>{title}</Button>
    )
}


export const WithdrawButton = ({ title, withdraw, background }: any) => {
    return (
        <Button onClick={() => withdraw()} style={{
            fontFamily,
            height: '2em',
            fontSize: '1.8em',
            width: '8em',
            borderRadius: '0.5em',
            background,
            color: '#FFFFFF' 
        }}>{title}</Button>
    )
}

export const WithdrawEarlyButton = ({ title, withdraw, background }: any) => {
    return (
        <Button onClick={() => withdraw()} style={{
            fontFamily,
            height: '2em',
            fontSize: '1.8em',
            width: '10em',
            borderRadius: '0.5em',
            background,
            color: '#FFFFFF' 
        }}>{title}</Button>
    )
}
