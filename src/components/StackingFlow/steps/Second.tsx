import { useState } from "react";

import { Typography } from "antd";

import { useWindowSize } from "hooks";
import { black, color, fontFamily } from "layout/constants";

import { StepButton2 } from "./StepButton";
const { Text } = Typography;

const styles = {
  container: {
    width: "80%",
    minWidth: "330px",
    maxWidth: "588px",
    textAlign: "center",
    margin: "auto",
    padding: "20px 0",
    borderRadius: "10px"
  },
  input: {
    color: black
  }
} as const;

const plans = [
    { id: 1, number: 3, time: 'MONTHS', apr: -0.11, ewp: 66 },
    { id: 2, number: 6, time: 'MONTHS', apr: -0.22, ewp: 66 },
    { id: 3, number: 1, time: 'YEARS', apr: -0.66, ewp: 66 },
    { id: 4, number: 5, time: 'YEARS', apr: -1.2, ewp: 66 },
]

export const SecondStep = ({ setPlan }: any) => {
    const { isTablet } = useWindowSize();
    const [selected, setSelect] = useState(0);

    return (
        <div style={{ ...styles.container, width: isTablet ? "90%" : "80%" }}>
            <div style={{ 
                display: 'flex',
                flexDirection: 'column',
                border: '0.25em solid #542297',
                padding: '0 2.625em 3em 2.625em',
                color
            }}>
                <div style={{ paddingTop: '0.625em', display: 'flex', flexDirection: 'row', justifyContent: 'left', width: '100%' }}>
                    <Text style={{ color, fontFamily, fontSize: isTablet ? '2em' : '3em', fontWeight: 400 }}>STEP 2:&nbsp;</Text>
                </div>
                <Text style={{ textAlign: 'left', opacity: 0.5, color, fontFamily, fontSize: isTablet ? '1.2em' : '2em', fontWeight: 300 }}>CHOOSE PLAN</Text>

                <div style={{ height: '0.5em' }} />

                {plans.map(plan => {
                    return (
                        <>
                            <div 
                                onClick={() => setSelect(plan.id)}

                                style={{
                                    cursor: 'pointer',
                                    display: 'flex', 
                                    flexDirection: 'row', 
                                    justifyContent: 'space-between', 
                                    border: '2.67px solid #4E4E4E', 
                                    padding: '0 1em 0 1em', 
                                    textAlign: 'right',
                                    background: selected === plan.id ? 'linear-gradient(269.98deg, #542297 0.22%, #32155B 99.98%)' : 'none'
                                }}
                            > 
                                <div>
                                    <Text style={{ color, fontFamily, fontSize: isTablet ? '2em' : '3em', fontWeight: 400 }}>{plan.number}</Text>
                                    <Text style={{ opacity: 0.7, color, fontFamily, fontSize: isTablet ? '1.5em' : '2em', fontWeight: 300 }}>&nbsp;{plan.time}</Text>
                                </div>

                                <div style={{ paddingTop: '0em' }}>
                                    <div>
                                        <Text style={{ opacity: 0.7, color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300 }}>APR: </Text>
                                        <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 400 }}>{plan.apr}%</Text>
                                    </div>
                                    <div>
                                        <Text style={{ opacity: 0.7, color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 300 }}>EWP: </Text>
                                        <Text style={{ color, fontFamily, fontSize: isTablet ? '1em' : '1.5em', fontWeight: 400 }}>{plan.ewp}%</Text>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                })}

                <div style={{ height: '0.5em' }} />
                <Text style={{ textAlign: 'left', opacity: 0.5, color, fontFamily, fontSize: isTablet ? '1em' : '1.4em', fontWeight: 300 }}>EWP - Early Withdrawal Penalty</Text>
                <div style={{ height: '2em' }} />

                <div>
                    <StepButton2 title={'STEP 3'} setPlan={setPlan} plan={selected} />
                </div>
            </div>
        </div>
    );
}
