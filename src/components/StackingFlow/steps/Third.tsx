
// import { useWeb3React } from "@web3-react/core";
import { useEffect, useState } from "react";

import { useWeb3React } from "@web3-react/core";
import { Typography } from "antd";
import { ethers } from "ethers";

import { StackingInfo } from "components/Common/StackingInfo";
import { stackingABI, stackingAddress, tokenABI, tokenAddress } from "components/Common/utils";
import { waitConfirmation } from "components/Common/waitingConfirmation";
import { useSignerOrProvider, useWindowSize } from "hooks";
import { black, color, fontFamily } from "layout/constants";

import { StakeButton } from "./StepButton";

const { Text } = Typography;

const styles = {
  container: {
    width: "80%",
    minWidth: "330px",
    maxWidth: "588px",
    textAlign: "center",
    margin: "auto",
    padding: "20px 0",
    borderRadius: "10px"
  },
  input: {
    color: black
  }
} as const;

export const ThirdStep = ({ amount, plan }: any) => {
    const { account } = useWeb3React();
    const { signer, provider } = useSignerOrProvider();
    const { isTablet } = useWindowSize();
    const [isRefresh, setRefresh] = useState(true);
    const [isShouldApprove, setShouldApprove] = useState(false);

    const approve = async () => {
        const tokenContract = new ethers.Contract(tokenAddress, tokenABI.abi, signer)
        const tx = await tokenContract.approve(stackingAddress, ethers.constants.MaxUint256)
        await waitConfirmation(provider, tx.hash)
        setRefresh(true)
    }

    const plans = {
        1: '3 month',
        2: '6 months',
        3: '1 years',
        4: '5 years',
    }

    const plansAPR = {
        1: { year: 0.25, percent: 0.11 },
        2: { year: 0.5, percent: 0.22 },
        3: { year: 1, percent: 0.66 },
        4: { year: 5, percent: 1.2 },
    }

    // @ts-expect-error skip
    const { year, percent } = plansAPR[plan]

    const data = {
        deposit: amount,
        withdrawal: Number((amount - (amount * percent / 100 * year)).toFixed(4)),
        // @ts-expect-error skip
        finishDate: new Date(new Date() + (31536000 * plansAPR[plan].year)),
        // @ts-expect-error skip
        period: plans[plan],
    }

    const stake = async () => {
        const staclingContract = new ethers.Contract(stackingAddress, stackingABI.abi, signer)
        await staclingContract.deposit(String(plan - 1), ethers.utils.parseEther(String(amount)).toString())

        setTimeout(() => {
            window.location.href = '/dashboard'
        }, 5000)
    }

    useEffect(() => {
        const check = async () => {
            const tokenContract = new ethers.Contract(tokenAddress, tokenABI.abi, signer)
            const allowance = await tokenContract.callStatic.allowance(account, stackingAddress)
            if (allowance.gt(ethers.utils.parseEther(String(amount)))) {
                setShouldApprove(false)
            } else {
                setShouldApprove(true)
            }
        }

        if (isRefresh) {
            setRefresh(false)
            check()
        }
        
    }, [account, isRefresh])

    return (
        <div style={{ ...styles.container, width: isTablet ? "90%" : "80%" }}>
            <div style={{ 
                display: 'flex',
                flexDirection: 'column',
                border: '0.25em solid #542297',
                padding: '0em 2.625em 3em 2.625em',
                color,
                background: 'linear-gradient(0deg, #7A30DC, #7A30DC)'

            }}>
                <div style={{ paddingTop: '0.625em', display: 'flex', flexDirection: 'row', justifyContent: 'left', width: '100%' }}>
                    <Text style={{ color, fontFamily, fontSize: isTablet ? '2em' : '3em', fontWeight: 400 }}>STEP 3:&nbsp;</Text>
                </div>
                <Text style={{ textAlign: 'left', opacity: 0.9, color, fontFamily, fontSize: isTablet ? '1.2em' : '2em', fontWeight: 300 }}>CONFIRMATION</Text>

                <div style={{ height: '0.5em' }} />

                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <StackingInfo data={data} />
                    <div style={{ height: '1em' }} />
                    <div style={{ textAlign: 'left', display: 'flex', flexDirection: 'row', justifyContent: 'space-between'  }}>
                        <div style={{ maxWidth: '15em' }}>
                            <Text style={{ color }}>NOTICE: </Text>
                            <br></br>
                            <Text style={{ color }}>By making a deposit you agree to all the terms and conditions of GRIMACE STAKING</Text>
                        </div>
                        <div></div>
                    </div>
                </div>
         
                <div style={{ height: '2em' }} />

                <div>
                    {isShouldApprove ? <StakeButton title={'APPROVE'} stake={approve} /> : <StakeButton title={'STAKE'} stake={stake} />}
                </div>
            </div>

            <div style={{ height: '0.6em' }} />
        </div>
    );
}
