import { Typography } from "antd";

import bitget_Logo from "assets/svg/grimace/bitget.svg";
import swap_Logo from "assets/svg/grimace/swap.svg";
import { ConnectAccount } from "components/Account/ConnectAccount";
import { useWindowSize } from "hooks";
import { black, color, fontFamily } from "layout/constants";
const { Text } = Typography;

const styles = {
  container: {
    width: "80%",
    minWidth: "330px",
    maxWidth: "588px",
    textAlign: "center",
    margin: "auto",
    padding: "20px 0",
    borderRadius: "10px"
  },
} as const;

export const IntroStep = () => {
    const { isTablet } = useWindowSize();

    return (
        <div style={{ ...styles.container, width: isTablet ? "90%" : "80%" }}>
            <div style={{ 
                display: 'flex',
                flexDirection: 'column',
                border: '0.25em solid #E68C36',
                padding: '1.875em 0.625em 3.125em 0.625em'
                }}>
                <div style={{ paddingTop: '0.625em', display: 'flex', flexDirection: 'row', justifyContent: 'center', width: '100%', }}>
                <Text style={{ color, fontFamily, fontSize: isTablet ? '2.5em' : '4em', fontWeight: 300 }}>STAKE&nbsp;</Text>
                <Text style={{ color, fontFamily, fontSize: isTablet ? '2.5em' : '4em', fontWeight: 500 }}>GRIMACE</Text>
                </div>
                <Text style={{ opacity: 0.5, color, fontFamily, fontSize: isTablet ? '1.4em' : '2.1875em', fontWeight: 300 }}>PROTECTING YOU FROM YOU</Text>

                <div style={{ paddingTop: '1.875em' }}>
                <ConnectAccount />
                </div>
            </div>

            <div style={{ height: '10px' }} />

            <div style={{
                backgroundColor: '#ffffff',
                padding: '0 0.7em 0 0.15em',
                border: '0.25em solid #055257',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                height: '6em'
            }}>
                <img src={bitget_Logo} style={{ width: '5.625em', height: '5.625em' }} alt="web3-wallet" />
                <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'right' }}>
                <div style={{ height: '3em', display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', color: black, fontFamily }}>BitGet&nbsp;</Text>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', fontWeight: 300, color: black, fontFamily }}>EXCHANGE</Text>
                </div>
                <Text style={{ opacity: .7, fontSize: isTablet ? '1em' : '1.25em', fontWeight: 300, color: black, fontFamily }}>BUY GRIMACE TOKEN ON BITGET CEX</Text>
                </div>
            </div>

            <div style={{ height: '10px' }} />

            <div style={{
                background: 'linear-gradient(90deg, #7A30DC -3.06%, #271145 100%)',
                padding: '0 0.7em 0 0.15em',
                border: '0.25em solid #32155B',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                height: '6em'
            }}>
                <img src={swap_Logo} style={{ width: '5.625em', height: '5.625em' }} alt="web3-wallet" />

                <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'right' }}>
                <div style={{ height: '3em', display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', color, fontFamily }}>GRIMACE&nbsp;</Text>
                    <Text style={{ fontSize: isTablet ? '1.7em' : '2.1875em', fontWeight: 300, color, fontFamily }}>SWAP</Text>
                </div>
                    <Text style={{ opacity: .7, fontSize: isTablet ? '1em' : '1.25em', fontWeight: 300, color, fontFamily }}>SWAP GRIMACE ON ARBITRUM</Text>
                </div>
            </div>
        </div>
    );
}
